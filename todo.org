* TODO Auto-caption?
* DONE Fix quotes incl. newlines
	CLOSED: [2020-05-19 Tue 23:10]
* DONE Throttle CPU via queueing
	CLOSED: [2020-05-17 Sun 15:47]
* DONE Audio stuttering
	CLOSED: [2020-05-15 Fri 13:10]
* DONE Use new celebration slide
	CLOSED: [2020-05-14 Thu 23:39]
* DONE Redo header video - aerial & KC photo
	CLOSED: [2020-05-14 Thu 23:11]
* DONE Get name, major, awards from other spreadsheet
	CLOSED: [2020-05-14 Thu 22:51]
* DONE Still too much fucking ducking
	CLOSED: [2020-05-14 Thu 22:09]
* DONE Build header video
	CLOSED: [2020-05-11 Mon 23:23]
* DONE Audio ducking weird again
	CLOSED: [2020-05-11 Mon 23:23]
* DONE Fix failures after celebration auto-crop/scale feature
	CLOSED: [2020-05-11 Mon 19:39]
* DONE Better error handling
	CLOSED: [2020-05-10 Sun 14:40]
	- Silence ffmpeg junk on stderr
	- Show stderr
	- Reject and fail if any step fails
	- Log failures to file
* DONE crop celebration video
	CLOSED: [2020-05-10 Sun 11:11]
* DONE Duck music even lower
	CLOSED: [2020-05-10 Sun 10:44]
* DONE kcook scale/crop issue
	CLOSED: [2020-05-09 Sat 23:57]
* DONE Double entries for acacia@keuka.edu
	CLOSED: [2020-05-09 Sat 23:56]
* DONE Handle missing video/image
	CLOSED: [2020-05-09 Sat 22:23]
* DONE Don't generate a final video at all if we have one
	CLOSED: [2020-05-09 Sat 15:25]
* DONE Fix rate limiting
	CLOSED: [2020-05-09 Sat 15:21]
* DONE Don't re-download assets
	CLOSED: [2020-05-09 Sat 14:26]
* DONE Fade ending
	CLOSED: [2020-05-09 Sat 14:26]
* DONE rflanagan audio sync
	CLOSED: [2020-05-09 Sat 12:17]
* DONE Duck audio
	CLOSED: [2020-05-09 Sat 11:32]
* DONE Parse TSV
	CLOSED: [2020-05-05 Tue 21:28]
* DONE Layer image and slide layers
	CLOSED: [2020-05-08 Fri 21:44]
* DONE Crossfade video
	CLOSED: [2020-05-08 Fri 21:44]
