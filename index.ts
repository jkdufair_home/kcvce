const fs = require('fs');
const readline = require('readline');
const { google } = require('googleapis');
const { spawn } = require('child_process');
const concat = require('ffmpeg-concat');

// If modifying these scopes, delete token.json.
const SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly',
	'https://www.googleapis.com/auth/drive'];
// The file token.json stores the user's access and refresh tokens, and is
// created automatically when the authorization flow completes for the first
// time.
const TOKEN_PATH = 'token.json';

// Load client secrets from a local file.
fs.readFile('credentials.json', (err: string, content: string) => {
	if (err) return console.error('Error loading client secret file:', err);
	// Authorize a client with credentials, then call the Google Sheets API.
	authorize(JSON.parse(content), processNewRows);
});

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback) {
	const { client_secret, client_id, redirect_uris } = credentials.installed;
	const oAuth2Client = new google.auth.OAuth2(
		client_id, client_secret, redirect_uris[0]);

	// Check if we have previously stored a token.
	fs.readFile(TOKEN_PATH, (err: string, token: string) => {
		if (err) return getNewToken(oAuth2Client, callback);
		oAuth2Client.setCredentials(JSON.parse(token));
		callback(oAuth2Client);
	});
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback for the authorized client.
 */
function getNewToken(oAuth2Client, callback) {
	const authUrl = oAuth2Client.generateAuthUrl({
		access_type: 'offline',
		scope: SCOPES,
	});
	console.log('Authorize this app by visiting this url:', authUrl);
	const rl = readline.createInterface({
		input: process.stdin,
		output: process.stdout,
	});
	rl.question('Enter the code from that page here: ', (code: string) => {
		rl.close();
		oAuth2Client.getToken(code, (err: string, token: string) => {
			if (err) return console.error('Error while trying to retrieve access token', err);
			oAuth2Client.setCredentials(token);
			// Store the token to disk for later program executions
			fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err: string) => {
				if (err) return console.error(err);
				console.log('Token stored to', TOKEN_PATH);
			});
			callback(oAuth2Client);
		});
	});
}

const PROCESS_DIR_NAME = 'process';
const OUT_DIR_NAME = 'out';
const NAME_CAPTION_FILE_NAME = 'name-caption.png';
const MAJOR_CAPTION_FILE_NAME = 'major-caption.png';
const QUOTE_CAPTION_FILE_NAME = 'quote-caption.png';
const STATIC_IMAGE_NO_NAME_FILE_NAME = "Virtual Commencement_Horizontal Slide_JUST NAME.jpg";
const STATIC_SLIDE_VIDEO_FILE_NAME = 'static-slide.mp4';
const CELEBRATION_SLIDE_VIDEO_FILE_NAME = 'celebration-slide.mp4';
const CELEBRATION_SLIDE_AUDIO_FILE_NAME = 'celebration-slide.wav';
const SILENT_FINAL_VIDEO_FILE_NAME = 'final-silent.mp4';
const MERGED_AUDIO_FILE_NAME = 'merged.m4a';
const FINAL_SLIDE_MUSIC = "Vital - A New Galaxy.wav";
const INTRO_SLIDE_LENGTH = 7560;
const STATIC_SLIDE_LENGTH = 5;
const CROSSFADE_TIME = 200;

function personProcessFile(emailAddress: string, filename: string) {
	return `${PROCESS_DIR_NAME}/${emailAddress}/${filename}`
}

interface TypedFilename {
	type: string,
	filename: string | null
}

function getFileFromDrive(auth, fileId: string, type: string, filename: string, emailAddress: string, dest,
													attemptNumber: number, resolve: (fileData: TypedFilename) => void, reject: (err: string) => void) {
	const drive = google.drive({ version: 'v3', auth });
	drive.files.get(
		{ fileId, alt: 'media' },
		{ responseType: 'stream' },
		(err: string, res) => {
			if (err) {
				if (attemptNumber < 10)
					setTimeout(() => {
						console.log(`${emailAddress}: Retrying getFileFromDrive. Attempt number ${attemptNumber}`);
						getFileFromDrive(auth, fileId, type, filename, emailAddress, dest, attemptNumber + 1, resolve, reject);
					}, attemptNumber**2 * 1000)
				else
					reject(`Error downloading file ${filename}: ${err}`);
			}
			if (!res)
					reject(`Result is undefined`)
			res.data
				.on('end', () => {
					console.log(`${emailAddress}: Done downloading ${filename}`);
					resolve({ type, filename });
				})
				.pipe(dest);
		}
	)
}

function getFilename(auth, fileId: string, emailAddress: string, attemptNumber: number,
										 resolve: (filename: string) => void, reject: (err: string) => void) {
	const drive = google.drive({ version: 'v3', auth });
	drive.files.get(
		{ fileId },
		(err: string, res: { data: { name: string; }; }) => {
			if (err) {
				if (attemptNumber < 10)
					setTimeout(() => {
						console.log(`${emailAddress}: Retrying getFilename. Attempt number ${attemptNumber}`);
						getFilename(auth, fileId, emailAddress, attemptNumber + 1, resolve, reject);
					}, attemptNumber**2 * 1000);
				else
				{
					reject(`Error getting filename for ${fileId} ${err}`);
				}
			} else {
				resolve(res.data.name);
			}
		}
	)
}

function downloadFile(auth, fileUrl: string, type: string, emailAddress: string,
											resolve: (fileData: TypedFilename) => void, reject: (err: string) => void) {
	if (!fileUrl) {
		resolve({ type, filename: null });
		return;
	}
	const fileId = fileUrl.split('=')[1];
	new Promise<string>((resolve, reject) => {
		getFilename(auth, fileId, emailAddress, 1, resolve, reject);
	})
		.then((filename: string) => {
			const filenameWithPath = personProcessFile(emailAddress, filename);
			if (!fs.existsSync(filenameWithPath)) {
				const dest = fs.createWriteStream(filenameWithPath);
				console.log(`${emailAddress}: downloading ${filename}`);
				getFileFromDrive(auth, fileId, type, filename, emailAddress, dest, 1, resolve, reject);
			} else {
				console.log(`${emailAddress}: ${filename} exists already`)
				resolve({ type, filename });
			}

		})
		.catch(err => reject(err));
}

function downloadFiles(auth, { selfieImageUrl, celebrationVideoUrl }, emailAddress: string,
											 resolve: (files: [TypedFilename, TypedFilename]) => void, reject: (err: string) => void) {
	var selfieDownloadPromise = new Promise<TypedFilename>((resolve) => {
		downloadFile(auth, selfieImageUrl, 'selfieImage', emailAddress, resolve, reject);
	})

	var celebrationDownloadPromise = new Promise<TypedFilename>((resolve) => {
		downloadFile(auth, celebrationVideoUrl, 'celebrationVideo', emailAddress, resolve, reject);
	})

	Promise.all([selfieDownloadPromise, celebrationDownloadPromise])
		.then((value: [TypedFilename, TypedFilename]) => {
			resolve(value);
		})
		.catch(err => {
			reject(err);
		})
}

function generateStaticSlideNameOverlay(name: string, emailAddress: string,
																				resolve: () => void, reject: (err: string) => void) {
	console.log(`${emailAddress}: Generating name caption`);
	const convert = spawn('convert', [
		"-size", "866x381",
		"-pointsize", "76",
		"-fill", "#83a42b",
		"-background", "none",
		"-gravity", "west",
		"-font", "/Users/jase/Library/Fonts/Raleway-ExtraBold.ttf",
		`caption:${name}`,
		"-flatten",
		personProcessFile(emailAddress, NAME_CAPTION_FILE_NAME)
	]);

	convert.stderr.on('data', (data: number[]) => {
		reject(`Error generating static slide name overlay: ${data.toString()}`);
	});

	convert.on('close', () => {
		console.log(`${emailAddress}: Name caption generated`);
		resolve();
	});
}

function generateStaticSlideMajorOverlay(major: string, emailAddress: string,
																				 resolve: () => void, reject: (err: string) => void) {
	console.log(`${emailAddress}: Generating major caption`);
	const convert = spawn('convert', [
		"-size", "866x248",
		"-pointsize", "45.83",
		"-fill", "#e89719",
		"-background", "none",
		"-font", "/Users/jase/Library/Fonts/RobotoSlab-Regular.ttf",
		`caption:${major}`,
		"-flatten",
		personProcessFile(emailAddress, MAJOR_CAPTION_FILE_NAME)
	]);

	convert.stderr.on('data', (data: number[]) => {
		reject(`Error generating static slide major overlay: ${data.toString()}`);
	})

	convert.on('close', () => {
		console.log(`${emailAddress}: Major caption generated`);
		resolve();
	});
}

function generateStaticSlideQuoteOverlay(quote: string, emailAddress: string, generateNarrowText: boolean) {
	return new Promise((resolve, reject) => {
		console.log(`${emailAddress}: Generating quote caption`);
		const convert = spawn('convert', [
			"-size", generateNarrowText ? "782x526" : "1200x600",
			"-pointsize", "48.55",
			"-fill", "#476239c0",
			"-background", "none",
			"-font", "/Users/jase/Library/Fonts/Raleway-Italic.ttf",
			`caption:${quote ?? ''}`,
			"-interline-spacing", "25",
			"-flatten",
			personProcessFile(emailAddress, QUOTE_CAPTION_FILE_NAME)
		]);

		convert.stderr.on('data', (data: number[]) => {
			reject(`Error generating static slide quote overlay: ${data.toString()}`);
		})

		convert.on('close', () => {
			console.log(`${emailAddress}: Quote caption generated`);
			resolve();
		});
	})
}

function generateNoNameStaticSlideVideo(emailAddress: string,	resolve: () => void, reject: (err: string) => void) {
	const ffmpeg = spawn('ffmpeg', [
		"-y",
		"-loglevel", "error",
		"-i", STATIC_IMAGE_NO_NAME_FILE_NAME,
		"-i", personProcessFile(emailAddress, NAME_CAPTION_FILE_NAME),
		"-loop", "1", "-i", personProcessFile(emailAddress, MAJOR_CAPTION_FILE_NAME),
		"-filter_complex",
		` \
[0:v][1:v]overlay=974:119[cap1], \
[2:v]fade=in:s=20:d=1.3:alpha=1[cap2], \
[cap1][cap2]overlay=974:477`,
		"-pix_fmt", "yuv420p",
		"-r", "25",
		"-t", STATIC_SLIDE_LENGTH,
		personProcessFile(emailAddress, STATIC_SLIDE_VIDEO_FILE_NAME)
	], { cwd: process.cwd() });

	ffmpeg.stderr.on('data', (data: number[]) => {
		reject(`Error generating static slide video: ${data.toString()}`);
	})

	ffmpeg.on('close', () => {
		console.log(`${emailAddress}: Static slide video generated`);
		resolve();
	});
}

function generateStaticSlideVideo(emailAddress: string, imageFilename: string,
																	resolve: () => void, reject: (err: string) => void) {
	console.log(`${emailAddress}: Generating static slide video`);
	if (!imageFilename) {
		generateNoNameStaticSlideVideo(emailAddress, resolve, reject);
	} else {
		const personImageFilename = personProcessFile(emailAddress, imageFilename);
		const mogrify = spawn('mogrify', [
			'-auto-orient',
			personImageFilename
		]);

		mogrify.stderr.on('data', (data: number[]) => {
			reject(`Error generating static slide video: ${data.toString()}`);
		});

		mogrify.on('close', () => {
			const convert = spawn('convert', [
				personImageFilename,
				"-format", "%[fx:w/h]\n",
				"info:"
			]);

			convert.stderr.on('data', (data: number[]) => {
				reject(`Error getting selfie dimensions: ${data.toString()}`);
			})

			let ratio: number;
			convert.stdout.on('data', (data: number[]) => { ratio = parseFloat(data.toString()); });

			convert.on('close', () => {
				const scaleExpression = ratio > 612/1080 ? "-2:1080" : "612:-2";
				const ffmpeg = spawn('ffmpeg', [
					"-y",
					"-loglevel", "error",
					"-i", personProcessFile(emailAddress, imageFilename),
					"-i", !!imageFilename ? "static-top-layer.png" : STATIC_IMAGE_NO_NAME_FILE_NAME,
					"-i", personProcessFile(emailAddress, NAME_CAPTION_FILE_NAME),
					"-loop", "1", "-i", personProcessFile(emailAddress, MAJOR_CAPTION_FILE_NAME),
					"-filter_complex",
					` \
[0:v]scale=${scaleExpression}[scaled], \
[scaled]crop=612:1080:y=0[scaled-cropped], \
[scaled-cropped]pad=1920:1080:x=186:y=0[padded], \
[padded][1:v]overlay=[slide], \
[slide][2:v]overlay=974:119[cap1], \
[3:v]fade=in:s=20:d=1.3:alpha=1[cap2], \
[cap1][cap2]overlay=974:477`,
					"-pix_fmt", "yuv420p",
					"-r", "25",
					"-t", STATIC_SLIDE_LENGTH,
					personProcessFile(emailAddress, STATIC_SLIDE_VIDEO_FILE_NAME)
				], { cwd: process.cwd() });

				ffmpeg.stderr.on('data', (data: number[]) => {
					reject(`Error generating static slide video: ${data.toString()}`);
				})

				ffmpeg.on('close', () => {
					console.log(`${emailAddress}: Static slide video generated`);
					resolve();
				});
			});
		});
	}
}

function generateCelebrationSlideVideo(emailAddress: string, videoFilename: string, hasQuote: boolean,
																			 resolve: () => void, reject: (err: string) => void) {
	console.log(`${emailAddress}: Generating celebration slide video`);
	const personVideoFilename = personProcessFile(emailAddress, videoFilename);
	const ffprobe = spawn('ffprobe', [
		"-v", "error",
		"-select_streams", "v:0",
		"-show_entries", "stream=width,height",
		"-show_entries", "stream_tags=rotate",
		"-of", "csv=s=x:p=0",
		personVideoFilename
	], { cwd: process.cwd() });

	ffprobe.stderr.on('data', (data: number[]) => {
		reject(`Error getting celebration video dimensions: ${data.toString()}`);
	})

	let ratio: number;
	ffprobe.stdout.on('data', (data: number[]) => {
		const [width, height, rotate] = data.toString().split('x');
		ratio = parseInt((rotate ?? "0") == "0" ? width : height) /
			parseInt((rotate ?? "0") == "0" ? height : width);
	})

	ffprobe.on('close', () => {
		const scaleExpression = ratio > 612/1080 ? "-2:1080" : "612:-2";

		const ffmpeg = spawn('ffmpeg', [
			"-y",
			"-loglevel", "error",
			"-loop", "1", "-i", hasQuote ? "video-lower-layer.png" : "video-lower-layer-no-quote.png",
			"-i", personProcessFile(emailAddress, QUOTE_CAPTION_FILE_NAME),
			"-i", personProcessFile(emailAddress, videoFilename),
			"-filter_complex",
			` \
[0][1:v]overlay=116:320[quoted], \
[2]scale=${scaleExpression}[scaled], \
[scaled]crop=612:1080:y=0[celebration], \
[quoted][celebration]overlay=1122:0:shortest=1`,
			"-r", "25",
			personProcessFile(emailAddress, CELEBRATION_SLIDE_VIDEO_FILE_NAME)
		], { cwd: process.cwd() });

		ffmpeg.stderr.on('data', (data: number[]) => {
			reject(`Error generating celebration slide video: ${data.toString()}. Ratio: ${ratio}`);
		});

		ffmpeg.on('close', () => {
			console.log(`${emailAddress}: Celebration slide video generated`);
			resolve();
		});
	})
}

function generateQuotationSlideVideo(emailAddress: string,
																		 resolve: () => void, reject: (err: string) => void) {
	console.log(`${emailAddress}: Generating quotation slide video`);
	const ffmpeg = spawn('ffmpeg', [
		"-y",
		"-loglevel", "error",
		"-loop", "1", "-i", "quote-lower-layer.png",
		"-i", personProcessFile(emailAddress, QUOTE_CAPTION_FILE_NAME),
		"-filter_complex",
		" \
[0][1]overlay=361:351",
		"-r", "25",
		"-t", STATIC_SLIDE_LENGTH,
		personProcessFile(emailAddress, CELEBRATION_SLIDE_VIDEO_FILE_NAME)
	], { cwd: process.cwd() });

	ffmpeg.stderr.on('data', (data: number[]) => {
		reject(`Error generating quotation slide video: ${data.toString()}`);
	});

	ffmpeg.on('close', () => {
		console.log(`${emailAddress}: Quotation slide video generated`);
		resolve();
	});
}

function extractCelebrationAudio(emailAddress: string, videoFilename: string,
																 resolve: () => void, reject: (err: string) => void) {
	const ffExtractAudio = spawn('ffmpeg', [
		"-y",
		"-loglevel", "error",
		"-i", personProcessFile(emailAddress, videoFilename),
		"-vn",
		"-acodec", "pcm_s16le",
		"-ar", "44100",
		personProcessFile(emailAddress, CELEBRATION_SLIDE_AUDIO_FILE_NAME)
	]);

	ffExtractAudio.stderr.on('data', (data: number[]) => {
		reject(`Error extracting celebration slide audio: ${data.toString()}`);
	});

	ffExtractAudio.on('close', () => {
		console.log(`${emailAddress}: Celebration slide audio extracted`);
		resolve();
	})
}

function combineFinalAudioAndVideo(emailAddress: string, audioFileName: string,
																	 resolve: () => void, reject: (err: string) => void) {
	const ffmpeg = spawn('ffmpeg', [
		"-y",
		"-loglevel", "error",
		"-i", personProcessFile(emailAddress, SILENT_FINAL_VIDEO_FILE_NAME),
		"-i", audioFileName,
		"-c:a", "aac",
		"-shortest",
		personProcessFile(emailAddress, 'unfaded.mp4')
	], { cwd: process.cwd() });

	ffmpeg.stderr.on('data', (data: number[]) => {
		reject(`Error generating unfaded video: ${data.toString()}`);
	});

	ffmpeg.on('close', () => {
		console.log(`${emailAddress}: Audio & video combined. Fading audio tail`)
		const ffFadeAudio = spawn('ffmpeg', [
			'-y',
			'-loglevel', 'error',
			'-i', personProcessFile(emailAddress, 'unfaded.mp4'),
			'-filter_complex', '[0:a]areverse[r]; [r]afade=d=2.5[r2]; [r2]areverse',
			`out/${emailAddress}.mp4`
		], { cwd: process.cwd() });

		ffFadeAudio.stderr.on('data', (data: number[]) => {
			reject(`Error fading audio tail: ${data.toString()}`);
		});

		ffFadeAudio.on('close', () => {
			resolve();
		});
	});
}

function concatenateAllVideos(emailAddress: string, hasQuote: boolean, hasCelebrationVideo: boolean) {
	console.log(`${emailAddress}: Concatenating videos`);

	return new Promise<void>((resolve, reject) => {
		// use at least the first slide with name/major/etc
		const videos = [
			'KCVCE-start.mov',
			personProcessFile(emailAddress, STATIC_SLIDE_VIDEO_FILE_NAME),
			'exit.mp4'
		];

		// if they have material for the second slide, insert it after the first slide
		(hasQuote || hasCelebrationVideo) && videos.splice(2, 0, personProcessFile(emailAddress, CELEBRATION_SLIDE_VIDEO_FILE_NAME));

		concat({
			output: personProcessFile(emailAddress, SILENT_FINAL_VIDEO_FILE_NAME),
			videos,
			transition: {
				name: 'fade',
				duration: 200
			}
		})
			.then(() => {
				if (hasCelebrationVideo) {
					console.log(`${emailAddress}: Ducking audio`)
					const voiceoverDelay = INTRO_SLIDE_LENGTH + STATIC_SLIDE_LENGTH * 1000 - CROSSFADE_TIME;
					const ffSideChain = spawn('ffmpeg', [
						"-y",
						"-loglevel", "error",
						"-i", FINAL_SLIDE_MUSIC,
						"-i", personProcessFile(emailAddress, CELEBRATION_SLIDE_AUDIO_FILE_NAME),
						"-filter_complex",
						` \
[1:a][1:a]amerge=inputs=2[stereo]; \
[stereo]loudnorm[ln]; \
[ln]apad[p]; \
[p]adelay=${voiceoverDelay}|${voiceoverDelay}[d]; \
[d]aformat=channel_layouts=stereo[s]; \
[s]asplit=2[sc][mix]; \
[0:a][sc]sidechaincompress=threshold=0.01:ratio=9:attack=750:release=1500:level_sc=2[bg]; \
[bg][mix]amerge`,
						personProcessFile(emailAddress, MERGED_AUDIO_FILE_NAME)
					], { cwd: process.cwd() });

					ffSideChain.stderr.on('data', (data: number[]) => {
						reject(`Error ducking audio: ${data.toString()}`);
					});

					ffSideChain.on('close', () => {
						console.log(`${emailAddress}: Audio ducked. Combining audio & video`)
						combineFinalAudioAndVideo(emailAddress, personProcessFile(emailAddress, MERGED_AUDIO_FILE_NAME), resolve, reject);
					});
				} else {
					combineFinalAudioAndVideo(emailAddress, FINAL_SLIDE_MUSIC, resolve, reject);
				}
			})
			.catch((err: string) => {
				reject(err);
			});
	});
}

function loadRegistrarData(auth: any) {
	return new Promise((resolve, reject) => {
		const sheets = google.sheets({ version: 'v4', auth });
		sheets.spreadsheets.values.get({
			spreadsheetId: '1gkjGFk5IKh-DwVY-YSd89mS0IM1DBoQfRELPFOrncfA',
			range: 'Sheet2!A1:C1'
		}, (err: string, res: { data: { values: any[] }; }) => {
			if (err)
				reject(err);

			resolve(res.data.values);
		});
	})
}

function getRegistrarRow(registrarData: any[], emailAddress: string) {
	const registrarRows = registrarData.filter(rd => rd[0] === emailAddress);
	if (registrarRows.length === 0) {
		console.error(`${emailAddress} not found in registrar data`);
		return null;
	}
	if (registrarRows.length > 1) {
		console.error(`${emailAddress}: multiple entries found in registrar data`);
		return null;
	}
	return registrarRows[0];
}

function processRow(auth: any, row: any, registrarData: any[]) {
	return new Promise((processRowResolve: any) => {
		const emailAddress = row[0];
		console.log(`Processing submitter ${emailAddress}`);
		if (fs.existsSync(`out/${emailAddress}.mp4`))	{
			console.log(`EXISTS -- ${emailAddress} -- EXISTS`);
			processRowResolve();
			return;
		}
		const selfieImageUrl = row[1];
		const celebrationVideoUrl = row[2];
		const quote = row[3];
		const registrarRow = getRegistrarRow(registrarData, emailAddress);
		if (!registrarRow) {
			console.log(`FAILED -- ${emailAddress} -- FAILED`);
			processRowResolve();
			return;
		}
		const graduationName = registrarRow[1];
		const major = registrarRow[2];

		const dirName = `${PROCESS_DIR_NAME}/${emailAddress}`;
		if (!fs.existsSync(dirName))
			fs.mkdirSync(dirName)

		const downloadPromise = new Promise((resolve, reject) => {
			downloadFiles(auth, { selfieImageUrl, celebrationVideoUrl }, emailAddress, resolve, reject);
		});
		const nameOverlayPromise = new Promise((resolve, reject) => {
			generateStaticSlideNameOverlay(graduationName.toUpperCase(), emailAddress, resolve, reject);
		});
		const majorOverlayPromise = new Promise((resolve, reject) => {
			generateStaticSlideMajorOverlay(major.toUpperCase(), emailAddress, resolve, reject);
		});

		let staticSlideVideoResolve: () => void;
		let staticSlideVideoReject: (err: string) => void;
		const staticSlideVideoPromise = new Promise((resolve, reject) => {
			staticSlideVideoResolve = resolve;
			staticSlideVideoReject = reject;
		});
		let celebrationSlideVideoResolve: () => void;
		let celebrationSlideVideoReject: (err: string) => void;
		const celebrationSlideVideoPromise = new Promise((resolve, reject) => {
			celebrationSlideVideoResolve = resolve;
			celebrationSlideVideoReject = reject;
		});
		let celebrationAudioExtractResolve: () => void;
		let celebrationAudioExtractReject: (err: string) => void;
		const celebrationAudioExtractPromise = new Promise((resolve, reject) => {
			celebrationAudioExtractResolve = resolve;
			celebrationAudioExtractReject = reject;
		});

		// We'll always have the "static" slide with their photo (or default), name, and major/awards
		Promise.all([downloadPromise, nameOverlayPromise, majorOverlayPromise])
			.then(values => {
				const selfieFilename = (values.filter(v => v instanceof Array)[0] as Array<TypedFilename>)[0].filename;
				generateStaticSlideVideo(emailAddress, selfieFilename, staticSlideVideoResolve, staticSlideVideoReject);
			})
			.catch(err => {
				console.error(`${emailAddress}: ${err}`);
				console.log(`FAILED -- ${emailAddress} -- FAILED`);
				processRowResolve();
			});

		// Once assets are downloaded, generate "celebration" slide if we have the assets
		let celebrationVideoFilename: string;
		downloadPromise
			.then((files: Array<TypedFilename>) => {
				celebrationVideoFilename = files.filter(f => f.type == 'celebrationVideo')[0].filename;

				// No video, no quote, don't generate a second slide
				if (!celebrationVideoFilename && !quote) {
					celebrationSlideVideoResolve();
					celebrationAudioExtractResolve();
				} else {
					// Generate quote text sized according to whether it will share the second slide with the video
					generateStaticSlideQuoteOverlay(quote, emailAddress, !!celebrationVideoFilename)
						.then(() => {
							if (celebrationVideoFilename) {
								generateCelebrationSlideVideo(emailAddress, celebrationVideoFilename, quote !== undefined, celebrationSlideVideoResolve, celebrationSlideVideoReject)
								extractCelebrationAudio(emailAddress, celebrationVideoFilename, celebrationAudioExtractResolve, celebrationAudioExtractReject);
							} else {
								generateQuotationSlideVideo(emailAddress, celebrationSlideVideoResolve, celebrationSlideVideoReject);
								celebrationAudioExtractResolve();
							}
						})
						.catch((err: string) => {
							console.error(`${emailAddress}: ${err}`);
							console.log(`FAILED -- ${emailAddress} -- FAILED`);
							processRowResolve();
						});
				}
			});

		Promise.all([staticSlideVideoPromise, celebrationSlideVideoPromise, celebrationAudioExtractPromise])
			.then(_ => {
				concatenateAllVideos(emailAddress, !!quote, !!celebrationVideoFilename)
					.then(() => {
						console.log(`DONE -- ${emailAddress} -- DONE.`);
						processRowResolve();
					})
					.catch((err: string) => {
						console.error(`${emailAddress}: ${err}`);
						console.log(`FAILED -- ${emailAddress} -- FAILED`);
						processRowResolve();
					});
			})
			.catch((err: string) => {
				console.error(`${emailAddress}: ${err}`);
				console.log(`FAILED -- ${emailAddress} -- FAILED`);
				processRowResolve();
			});
	})
}

function processNonSubmitterRow(row: any) {
	return new Promise((processRowResolve: any) => {
		const emailAddress = row[0];
		console.log(`Processing non-submitter ${emailAddress}`);
		if (fs.existsSync(`out/${emailAddress}.mp4`))	{
			console.log(`EXISTS -- ${emailAddress} -- EXISTS`);
			processRowResolve();
			return;
		}
		const graduationName = row[1];
		const major = row[2];

		const dirName = `${PROCESS_DIR_NAME}/${emailAddress}`;
		if (!fs.existsSync(dirName))
			fs.mkdirSync(dirName)

		const nameOverlayPromise = new Promise((resolve, reject) => {
			generateStaticSlideNameOverlay(graduationName.toUpperCase(), emailAddress, resolve, reject);
		});
		const majorOverlayPromise = new Promise((resolve, reject) => {
			generateStaticSlideMajorOverlay(major.toUpperCase(), emailAddress, resolve, reject);
		});

		let staticSlideVideoResolve: () => void;
		let staticSlideVideoReject: (err: string) => void;
		const staticSlideVideoPromise = new Promise((resolve, reject) => {
			staticSlideVideoResolve = resolve;
			staticSlideVideoReject = reject;
		});

		// We'll always have the "static" slide with their photo (or default), name, and major/awards
		Promise.all([nameOverlayPromise, majorOverlayPromise])
			.then(values => {
				generateStaticSlideVideo(emailAddress, null, staticSlideVideoResolve, staticSlideVideoReject);
			})
			.catch(err => {
				console.error(`${emailAddress}: ${err}`);
				console.log(`FAILED -- ${emailAddress} -- FAILED`);
				processRowResolve();
			});

		Promise.all([staticSlideVideoPromise])
			.then(_ => {
				concatenateAllVideos(emailAddress, false, false)
					.then(() => {
						console.log(`DONE -- ${emailAddress} -- DONE.`);
						processRowResolve();
					})
					.catch((err: string) => {
						console.error(`${emailAddress}: ${err}`);
						console.log(`FAILED -- ${emailAddress} -- FAILED`);
						processRowResolve();
					});
			})
			.catch((err: string) => {
				console.error(`${emailAddress}: ${err}`);
				console.log(`FAILED -- ${emailAddress} -- FAILED`);
				processRowResolve();
			});
	})
}

function processNewRows(auth: any) {
	loadRegistrarData(auth)
		.then((registrarData: any[]) => {
			const sheets = google.sheets({ version: 'v4', auth });
			sheets.spreadsheets.values.get({
				spreadsheetId: '1xdVfRD5lccnsJmAruVLlstbzSVR2jnPBr9SWVeeFyj4',
				range: 'Form Responses 1!B27:E27'
			}, (err: string, res: { data: { values: any[] }; }) => {
				if (err) return console.log('Sheets API returned an error: ' + err);
				const submitterData = res.data.values;
				const submitterEmailAddresses = submitterData.map(s => s[0]);
				const maxQueueLength = 6;
				let processedCount = 0;
				let queuedCount = 0;
				const interval = setInterval(() => {
					if (interval && processedCount === registrarData.length) {
						clearInterval(interval);
					}	else	{
						const batchMakeupCount = Math.min(maxQueueLength - queuedCount, registrarData.length - processedCount);
						for (let i = 0; i < batchMakeupCount; i++) {
							queuedCount++;
							const emailAddress = registrarData[processedCount + i][0]
							if (submitterEmailAddresses.includes(emailAddress))
								processRow(auth, submitterData.find(sd => sd[0] === emailAddress), registrarData)
									.then(() => {
										processedCount++;
										queuedCount--;
										console.log(`Processed: ${processedCount}, Queued: ${queuedCount}`);
									});
							else
								processNonSubmitterRow(registrarData.find(rd => rd[0] === emailAddress))
									.then(() => {
										processedCount++;
										queuedCount--;
										console.log(`Processed: ${processedCount}, Queued: ${queuedCount}`);
									});
						}
					}
				}, 1000);
			});
		})
		.catch((err: string) => {
			console.log(`Unable to fetch registrar data: ${err}`);
		});
}
